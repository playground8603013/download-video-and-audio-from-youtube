import yt_dlp


#youtube_dl config
YDL_OPTS_FOR_MP4 = {
            'format': 'bestvideo[height<=480]+bestaudio/best[height<=480]',
            'videoformat' : "mp4",
            'outtmpl': '%(title)s.%(ext)s',
            'postprocessors': [{
                'key': 'FFmpegVideoConvertor',
                'preferedformat': 'mp4',  # one of avi, flv, mkv, mp4, ogg, webm
            }]}
YDL_OPTS_FOR_MP3 = {
            'format': 'bestaudio/best', # choice of quality
            'extractaudio' : True,      # only keep the audio
            'audioformat' : 'mp3',      # convert to mp3
            'outtmpl': '%(title)s.%(ext)s',
            'postprocessors': [{
                'key': 'FFmpegExtractAudio',
                'preferredcodec': 'mp3',
                'preferredquality': '192',
                }],
    }

def download_audio(link):
    try:
        ydl = yt_dlp.YoutubeDL(YDL_OPTS_FOR_MP3)
        info = ydl.extract_info(link, download=False)
        ydl = yt_dlp.YoutubeDL(YDL_OPTS_FOR_MP3)
        ydl.download([link])
        info_with_audio_extension = dict(info)
        info_with_audio_extension['ext'] = 'mp3'
        return ydl.prepare_filename(info_with_audio_extension)
    except Exception as err:
        print(str(err))


def download_video(link):
    try:
        ydl = yt_dlp.YoutubeDL(YDL_OPTS_FOR_MP4)
        info = ydl.extract_info(link, download=False)
        ydl.download([link])
        info_with_video_extension = dict(info)
        info_with_video_extension['ext'] = 'mp4'
        return ydl.prepare_filename(info_with_video_extension)
    except Exception as err:
        print(str(err))

link = input("Enter the YouTube video URL: ")
download_type = input("Enter type of file(1:mp4,2:mp3)")
if download_type == "1":
    print("Downloading video....")
    download_video(link=link)
elif download_type == "2":
    print("Downloading audio....")
    download_audio(link)
else:
    print("Your input are mismatch!")